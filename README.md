# gitlab-runner-setup

Follow the simple steps when connected to the instance:

- Install `docker` and `gitlab-runner` start/enable the services.
- Run the following:

```
sudo gitlab-runner register -n \
  --url https://gitlab.mydomain.com/ \
  --registration-token abcd1234xyz456 \
  --executor docker \
  --description "Docker-in-Docker runner" \
  --tag-list "my-tag" \
  --docker-image "alpine:latest" \
  --docker-privileged \
  --docker-volumes "/certs/client"
```

- TLS certificates will be generated, and it is recommended to use them by setting the `DOCKER_TLS_CERTDIR` variable in the root of your `.gitlab-ci.yml`:

```
variables:
    DOCKER_TLS_CERTDIR: "/certs"
```

- Then use the `services` keyword (defines a Docker image that runs during a job linked to the Docker image that the image keyword defines) inside your job. You also need to use an image that sets the environment for Docker, e.g `docker:latest`.

```
build:
    stage: build
    image: docker:latest
    services:
        - docker:dind
```

- Finally, start/enable the `gitlab-runner` service.

